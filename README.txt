
-- SUMMARY --

Integrates the PET (Previewable E-mail Templates) module with Rules. Provides a 
single Rules condition to specify a PET name, actions can then be assigned to 
trigger when an e-mail is sent using that template.

This module is intended to be used with the PET, Rules and Token modules. If 
you pass a Node ID (nid) to a PET, all of that node's fields (including CCK 
fields) will be available as tokens to Rules actions. User object tokens are 
also available.


-- REQUIREMENTS --

PET - http://drupal.org/project/pet
Rules - http://drupal.org/project/rules
Token - http://drupal.org/project/token


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

This module has no configuration options of its own. Use the configuration 
pages provided by the PET and Rules module for functionality.

The module adds an option called "Select a PET template" to the list of 
conditions provided by Rules.


-- CONTACT

Current maintainers:
* Huw Roberts (wuh) - http://drupal.org/user/282303/

This project has been sponsored by:
* Rootsy
  A Cardiff web design & development company specialising in design and 
  development forDrupal.
