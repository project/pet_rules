<?php

/**
 * @file
 * Functions for rules integration.
 */

/**
 * Implements hook_rules_event_info().
 */
function pet_rules_rules_event_info() {
  return array(
    'pet_rules_email_sent' => array(
      'label' => t('E-mail sent via the PET module'),
      'module' => t('PET'),
      'arguments' => _pet_rules_event_variables(),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function pet_rules_rules_condition_info() {
  return array(
    'pet_rules_condition_template_name' => array(
      'label' => t('Select a PET template'),
      'arguments' => array(
        'template_name' => array('type' => 'value', 'label' => t('The name of the PET template used')),
        'selected_name' => array('label' => t('PET'), 'type' => 'string'),
      ),
      'help' => t('This condition just compares two texts. It returns TRUE, if both texts are equal.'),
      'module' => 'PET',
    ),
  );
}

/**
 * Compare the template name of the submitted PET with the selected name.
 */
function pet_rules_condition_template_name($template_name, $selected_name) {
  return $template_name == $selected_name;
}

/**
 * Generates the condition form to select a PET template.
 */
function pet_rules_condition_template_name_form($settings, &$form) {
  // Get a list of PETs that have been created.
  $pets = pet_get_pets();

  $pet_templates = array();
  $options = array();

  foreach ($pets as $pet) {
    $pet_name = $pet->name;
    $pet_title = $pet->title;
    $options[$pet_name] = $pet_title;
  }

  $form['settings']['selected_name'] = array(
    '#type' => 'select',
    '#title' => t('PET'),
    '#options' => $options,
    '#default_value' => isset($settings['selected_name']) ? $settings['selected_name'] : '',
    '#required' => TRUE,
  );
}

/**
 * Helper function for event variables.
 *
 * @return array
 *   All available variables for the rules events provided by pet_rules.
 */
function _pet_rules_event_variables() {
  return array(
    'user' => array(
      'type' => 'user',
      'label' => t('the user who submitted the PET'),
    ),
    'node' => array(
      'type' => 'node',
      'label' => t('the PET node'),
    ),
    'template_name' => array(
      'type' => 'string',
      'label' => t('the form id of the submitted form'),
      'hidden' => TRUE,
    ),
    'selected_name' => array(
      'type' => 'string',
      'label' => t('the selected PET template'),
      'hidden' => TRUE,
    ),
  );
}
